#include "image.h"
#include <stdio.h>

enum read_status {
	READ_OK = 0,
	READ_INVALID_HEADER,
	READ_INVALID_BITS,
	READ_ERROR
};

enum read_status reading_image(const char* const filename, struct image* const img, enum read_status (*from_format)(FILE* in, struct image* img));

enum write_status {
	WRITE_OK = 0,
	WRITE_INVALID_HEADER,
	WRITE_INVALID_BITS,
	WRITE_ERROR
};

enum write_status writing_image(const char* const filename, struct image* const img, enum write_status (*to_format)(FILE* out, struct image const* img));
