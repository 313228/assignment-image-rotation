#include <stdint.h>
#include <stddef.h>
#ifndef __IMAGE_H__ 

#define __IMAGE_H__ 

#pragma pack(push, 1)
struct pixel {uint8_t b,g,r;};
#pragma pack(pop)

struct image {
 uint64_t width, height;
 struct pixel* data;
};

static inline size_t coordinates_to_index(size_t x, size_t y, size_t height) {
    return y * height + x;
}

struct image create_image(uint32_t width, uint32_t height, struct pixel *data);

#endif
