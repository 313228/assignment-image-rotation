#include "bmp.h"
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    
    if (!fread(header, sizeof(struct bmp_header), 1, in)) {
        free(header);
        return READ_INVALID_HEADER;
    }
    else {
        uint32_t height = header -> biHeight;
        uint32_t width = header -> biWidth;

	free(header);

        struct pixel* pixel_array = malloc(sizeof(struct pixel) * height * width);
        
        for(size_t i = 0; i < height; i++) {
            if(fread(&(pixel_array [i * width]), sizeof(struct pixel), width, in) != width) {
                free(pixel_array);
                return READ_INVALID_BITS;
            }

            if (fseek(in, width % 4, SEEK_CUR)) {
                free(pixel_array);
                return READ_INVALID_BITS;
            }
        }

	*img = (struct image) {width, height, pixel_array};
        
    return READ_OK;
    }
}

static inline uint32_t file_size(uint32_t height, uint32_t width) {
    return sizeof(struct bmp_header) + sizeof(struct pixel) * width * height + height * (width % 4);
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    uint32_t height = img -> height;
    uint32_t width = img -> width;
    
    uint32_t off_bits = sizeof(struct bmp_header);
    uint32_t size = file_size(height, width);
    
    struct bmp_header header = {
        .bfType = 19778,
        .bfileSize = size,
        .bfReserved = 0,
        .bOffBits = off_bits,
        .biSize = (sizeof(struct bmp_header) - sizeof(uint16_t) - 3 * sizeof(uint32_t)),
        .biWidth = width,
        .biHeight = height,
        .biPlanes = 1,
        .biBitCount = sizeof(struct pixel) * 8,
        .biCompression = 0,
        .biSizeImage = size - off_bits,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
    
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_INVALID_HEADER;
    }
    else {
        const size_t padding = 0;
        for(size_t i = 0; i < height; i++) {
            if (fwrite(&(img->data[i * width]), sizeof(struct pixel), width, out) != width ||
                fwrite(&padding, sizeof(char), width % 4, out) != width % 4) {
                    return WRITE_INVALID_BITS;
                }
        }
        
        return WRITE_OK;
    }

}




