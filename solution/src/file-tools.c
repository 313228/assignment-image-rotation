#include "file-tools.h"
#include <stdbool.h>
#include <stdlib.h>

bool opening_file(const char* filename, const char* mode, FILE** file) {
    *file = fopen(filename, mode);

    if (!file) {
    	return false;
    }
    else {
	    return true;
    }
}

bool closing_file(FILE* file) {
    if (fclose(file) == EOF) {
	    return false;
    }
    else {
	    return true;
    }
}

enum read_status reading_image(const char* const filename, struct image* const img, enum read_status (*from_format)(FILE* in, struct image* img)){
    FILE* in;
    
    if (!opening_file(filename, "rb", &in)) {
        return READ_ERROR;
    }
    else {
        enum read_status status = from_format(in, img);
        if (closing_file(in)) {
            return status;
        }
        else {
            return READ_ERROR;
        }
    }
}

enum write_status writing_image(const char* const filename, struct image* const img, enum write_status (*to_format)(FILE* out, struct image const* img)){
    FILE* out;
    
    if (!opening_file(filename, "wb", &out)) {
        return WRITE_ERROR;
    }
    else {
        enum write_status status = to_format(out, img);
        if (closing_file(out)) {
            return status;
        }
        else {
            return WRITE_ERROR;
        }
    }
}
