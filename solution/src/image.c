#include "image.h"
#include <stdlib.h>

struct image create_image (uint32_t width, uint32_t height, struct pixel *data) {
    struct image new_image = {
        .width = width,
        .height = height,
        .data = data
    };
    return new_image;
}

