#include "bmp.h"
#include "transformation.h"
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (argc != 3) {
        printf("Invalid number of arguments");
        return -1;
    }

    struct image input;
    
    enum read_status read_stat = reading_image(argv[1], &input, from_bmp);
    
    if (read_stat != READ_OK) {
	if(input.data) {
		free(input.data);
	}

        printf("Incorrect file opening");
        return -2;
    }
    
    struct image output = rotate(input);

    free(input.data);
    
    enum write_status write_stat = writing_image(argv[2], &output, to_bmp);

    free(output.data);
        
    if (write_stat != WRITE_OK) {
        printf("Incorrect file writing");
        return -3;
    }
    
    return 0;
}

