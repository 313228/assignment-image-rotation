#include "transformation.h"
#include <stdlib.h>

struct image rotate (struct image const source) {
    uint32_t height = source.height;
    uint32_t width = source.width;
    struct pixel *data = malloc (sizeof (struct pixel) * height * width);

    for (size_t y = 0; y < width; y++) {
        for (size_t x = 0; x < height; ++x) {
            data[coordinates_to_index(x, y, height)] = source.data[coordinates_to_index(y, height - 1 -x, width)];
	    }
    }
    
    struct image rotated_image = create_image(height, width, data);

    return rotated_image;
}
